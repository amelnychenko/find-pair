import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {flip, hover} from './core/animations/animation';
import {CardStatesEnum, GameStatesEnum, HoverStatesEnum, IntervalDirectionEnum} from './core/enums/enum';
import {Service} from './core/services/service';
import {images} from './core/constants/constant';
import {ICard} from './core/interfaces/interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [flip, hover],
})
export class AppComponent implements OnInit {
  private _state$: BehaviorSubject<GameStatesEnum> = new BehaviorSubject<GameStatesEnum>(null);
  private _startTime: number = 0;
  private _pairs: number[] = [];
  private _flipInterval: number;
  private _timeInterval: number;

  public hoverStatesEnum: typeof HoverStatesEnum = HoverStatesEnum;
  public isStarted: boolean;
  public cards: ICard[];
  public results: Map<string, number>;
  public currentTime: number = 0;
  public hoverCardIndex: number;

  constructor(private cdr: ChangeDetectorRef,
              private service: Service) {
  }

  ngOnInit(): void {
    this.getCards();
    this.getResults();
    this.appStateHandler();
  }

  public onStart(): void {
    this._state$.next(GameStatesEnum.FLIP_TO_FRONT);
  }

  public onStop(): void {
    this._state$.next(GameStatesEnum.STOP);
  }

  public onEnter(index: number): void {
    if (this._startTime && this.cards[index].state === CardStatesEnum.BACK) {
      this.hoverCardIndex = index;
    }
  }

  public onLeave(): void {
    this.hoverCardIndex = null;
  }

  public onOpen(index: number): void {
    if (this._startTime && this.cards[index].state === CardStatesEnum.BACK) {
      this.cards[index].state = CardStatesEnum.FRONT;
      this.checkPair(index);
    }
  }

  private checkPair(index: number): void {
    this._pairs.push(index);
    if (this._pairs.length === 2) {
      const pairs: number[] = this._pairs;
      this._pairs = [];
      if (this.cards[pairs[0]].image !== this.cards[pairs[1]].image) {
        setTimeout(() => pairs.forEach((item: number) =>
          this.cards[item].state = CardStatesEnum.BACK), 800);
      } else if (!this.cards.some((card) => card.state === CardStatesEnum.BACK)) {
        this._state$.next(GameStatesEnum.FINISH);
      }
    }
  }

  private appStateHandler(): void {
    this._state$.asObservable().subscribe((state: GameStatesEnum) => {
      switch (state) {
        case GameStatesEnum.FLIP_TO_FRONT:
          this.isStarted = true;
          this.getCards();
          this.flipTo(CardStatesEnum.FRONT);
          this.startTimeInterval(5, IntervalDirectionEnum.BACKWARD);
          break;
        case GameStatesEnum.FLIP_TO_BACK:
          this.stopTimeInterval();
          this.flipTo(CardStatesEnum.BACK);
          break;
        case GameStatesEnum.PLAY:
          this.startTimeInterval(0, IntervalDirectionEnum.FORWARD);
          break;
        case GameStatesEnum.STOP:
          this.currentTime = 0;
          this.getCards();
          this.resetProgress();
          break;
        case GameStatesEnum.FINISH:
          this.service.result = this._startTime;
          this.getResults();
          this.resetProgress();
          break;
      }
      this.cdr.detectChanges();
    });
  }

  private flipTo(side: CardStatesEnum): void {
    this.stopFlipInterval();
    let index: number = 0;
    this._flipInterval = setInterval(() => {
      this.cards[index++].state = side;
      (index === this.cards.length) && this.stopFlipInterval();
      (side === CardStatesEnum.BACK) && this._state$.next(GameStatesEnum.PLAY);
      this.cdr.detectChanges();
    }, 40);
  }

  private startTimeInterval(startTime: number, direction: IntervalDirectionEnum): void {
    this.stopTimeInterval();
    this.currentTime = startTime;
    this._timeInterval = setInterval(() => {
      (direction === IntervalDirectionEnum.FORWARD && !this._startTime) && (this._startTime = Date.now());
      (direction === IntervalDirectionEnum.FORWARD) ? this.currentTime++ : this.currentTime--;
      (this.currentTime <= 0) && this._state$.next(GameStatesEnum.FLIP_TO_BACK);
      this.cdr.detectChanges();
    }, 1000);
  }

  private getCards(): void {
    this.cards = images.concat(images)
      .sort(() => Math.random() - 0.5)
      .map((image: string) => ({image, state: CardStatesEnum.BACK}));
  }

  private getResults(): void {
    this.results = this.service.results;
  }

  private resetProgress(): void {
    this.stopFlipInterval();
    this.stopTimeInterval();
    this._startTime = 0;
    this._pairs = [];
    this.isStarted = false;
  }

  private stopFlipInterval(): void {
    clearInterval(this._flipInterval);
  }

  private stopTimeInterval(): void {
    clearInterval(this._timeInterval);
  }
}
