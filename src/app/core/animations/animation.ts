import {animate, AnimationTriggerMetadata, state, style, transition, trigger} from '@angular/animations';
import {CardStatesEnum, HoverStatesEnum} from '../enums/enum';

export const flip: AnimationTriggerMetadata = trigger('flip', [
  state(CardStatesEnum.FRONT, style({
    transform: 'rotateY(180deg)'
  })),
  state(CardStatesEnum.BACK, style({
    transform: 'rotateY(0)'
  })),
  transition(`${CardStatesEnum.FRONT} => ${CardStatesEnum.BACK}`, animate('200ms ease-out')),
  transition(`${CardStatesEnum.BACK} => ${CardStatesEnum.FRONT}`, animate('200ms ease-in'))
]);

export const hover: AnimationTriggerMetadata = trigger('hover', [
  state(HoverStatesEnum.ENTER, style({
    'box-shadow': '0 0 7px -3px #000000'
  })),
  state(HoverStatesEnum.LEAVE, style({
    'box-shadow': '0 0 7px -5px #000000'
  })),
  transition(`${HoverStatesEnum.ENTER} => ${HoverStatesEnum.LEAVE}`, animate('40ms ease-out')),
  transition(`${HoverStatesEnum.LEAVE} => ${HoverStatesEnum.ENTER}`, animate('40ms ease-in'))
]);
