import {CardStatesEnum} from '../enums/enum';

export interface ICard {
  image: string;
  state: CardStatesEnum;
}
