import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class Service {
  private _storageKeyBestResult: string = 'Best result';
  private _storageKeyLastResult: string = 'Last result';

  public get results(): Map<string, number> {
    const results: Map<string, number> = new Map<string, number>();
    results.set(this._storageKeyBestResult, this.bestResult);
    results.set(this._storageKeyLastResult, this.lastResult);
    return results;
  }

  public set result(startTime: number) {
    if (startTime) {
      const result: number = Date.now() - startTime;
      this.lastResult = result;
      (!this.bestResult || result < this.bestResult) && (this.bestResult = result);
    }
  }

  private get bestResult(): number {
    return +localStorage.getItem(this._storageKeyBestResult) || 0;
  }

  private set bestResult(data: number) {
    localStorage.setItem(this._storageKeyBestResult, data.toString());
  }

  private get lastResult(): number {
    return +localStorage.getItem(this._storageKeyLastResult) || 0;
  }

  private set lastResult(data: number) {
    localStorage.setItem(this._storageKeyLastResult, data.toString());
  }
}
