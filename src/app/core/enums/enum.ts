export enum GameStatesEnum {
  FLIP_TO_FRONT = 'flip-to-front',
  FLIP_TO_BACK = 'flip-to-back',
  PLAY = 'play',
  STOP = 'stop',
  FINISH = 'finish',
}

export enum CardStatesEnum {
  FRONT = 'front',
  BACK = 'back',
}

export enum IntervalDirectionEnum {
  FORWARD = 'forward',
  BACKWARD = 'backward',
}

export enum HoverStatesEnum {
  ENTER = 'enter',
  LEAVE = 'leave',
}
